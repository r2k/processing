//Seb Lee-Delisle, http://www.openprocessing.org/sketch/47766
//Claus Neergaard, http://www.openprocessing.org/sketch/6245
int count = 200;
float[] xp = new float[count];
float[] yp = new float[count];
float[] xSpeed = new float[count];
float[] ySpeed = new float[count];
 
int radius = 3;
 
 
 
void setup() {  
   
size(1000, 700); 
smooth();
 
for (int i = 0; i < count; i=i+1){
 
 
 xp[i] = int(random(width));
 
 yp[i] = int(random(height));
 
 xSpeed[i] = random (-2,2); 
 
 ySpeed[i] = 0;
    
    
}
} 
 
void draw() { 
fill (1,1000);
rect (0,0,width,height);
 

lines();
}
 
void lines(){
   
  for (int i = 0; i < count; i = i + 1) {  
  for (int a = i; a < count; a = a+ 1) { 
  if (dist(xp[a], yp[a], xp[i], yp[i]) <100) { 
  line(xp[a], yp[a], xp[i], yp[i]); 
  stroke(000);
  strokeWeight(0.2);
}
}
}
}
 

 
 

  

    


