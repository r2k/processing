import megamu.mesh.*;

class Particle {
  float xCurr, yCurr;
  float xInit, yInit;
  float xo,yo;
  Particle(float xo_, float yo_) {
    xo = xo_;
    yo = yo_;
    float degreeTemp = random(360);
    float rTemp = random(40, 500);
    xInit = cos(radians(degreeTemp))*rTemp+xo;
    yInit = sin(radians(degreeTemp))*rTemp+yo;
    xCurr = xInit;
    yCurr = yInit;
  }
  void update() {
    float x0 = xCurr;
    float y0 = yCurr;
    float a = mouseX-x0;
    float b = mouseY-y0;
    float r = sqrt(a*a+b*b);
    float quer_fugir_x = xCurr-(a/r)*100/r;
    float quer_fugir_y = yCurr-(b/r)*100/r;
    float quer_voltar_x = (xInit-x0)/10;
    float quer_voltar_y = (yInit-y0)/10;
    xCurr = quer_fugir_x+quer_voltar_x;
    yCurr = quer_fugir_y+quer_voltar_y;
  }
  void display() {
    strokeWeight(1);
    stroke(0);
    point(xCurr, yCurr);
  }
  void reset() {
    float degreeTemp = random(360);
    float rTemp = random(10, 300);
    xInit = cos(radians(degreeTemp))*rTemp+xo;
    yInit = sin(radians(degreeTemp))*rTemp+yo;
  }
}


 
int total;
float xOffset, yOffset;
float [][] pos;
Particle [] particles;
Delaunay delaunay;
 
void setup() {
  size(400, 300);
  smooth();
  background(255);
  total = 1800;
  xOffset = width/2;
  yOffset = height/2;
  particles = new Particle[total];
  for (int i=0;i<total;i++) {
    particles[i] = new Particle(xOffset, yOffset);
  }
}
void draw() {
  background(255);
  for (int i=0;i<total;i++) {
    particles[i].update();
    particles[i].display();
  }
  pos = new float[total][2];
  for ( int j=0; j<pos.length;j++) {
    pos[j][0] = particles[j].xCurr;
    pos[j][1] = particles[j].yCurr;
  }
  delaunay = new Delaunay(pos);
  float[][] edges = delaunay.getEdges();
  for (int i=0; i<edges.length; i++)
  {
    float startX = edges[i][0];
    float startY = edges[i][1];
    float endX = edges[i][2];
    float endY = edges[i][3];
    float trans = 255-dist(startX, startY, endX, endY)*4;
    float sw = 5/(dist(startX, startY, endX, endY)+1);
    strokeWeight(sw);
    stroke(0, trans);
    line(startX, startY, endX, endY);
  }
}
 
void keyPressed() {
  if (key =='r') {
    for (int i=0;i<total;i++) {
      particles[i].reset();
    }
  }
}


